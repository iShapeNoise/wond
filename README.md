m## WOND - Wifi Offline Network Dongle

WOND is a RaspberryPi Zero W, that runs an offline Wifi Server with a hotspot for users to connect to. Furthermore, you have the option to connect this server to the internet and connect to the OS via ssh.

WOND has been created for Music Live Sessions in a group using FoxDot. Therefore, the Wifi details are set accordingly. Feel free to change SSID and Password after installation.
<br>

### How to use WOND
<br>

__How to get it started?__
<br>
Just plug the dongle into a power supplied USB slot, directly to a power adapter or into a Pc/Laptop, and off you go.
<br>
After a minute, you will be able to connect to a Wifi Hotspot with the name and password below:
<br>
<br>
SSID: __FoxDotSpot__
<br>
Password: __jamsession2020__
<br>

__Shutdown the Wifi Server OS:__
<br>
Press the little button longer than a second, and wait till the green LED is off.
<br>
Unplug the dongle.
<br>
<br>
__Thats it!__
<br>


### D.I.Y. Instructions ###

#### What do you need?

* Raspberry Pi Zero W - 19.49 €
* SD Card 32GB - 5.99 €
* Male USB to DIP Adapter Converter 4pin 2.54mm PCB Board - 1 €
* Micro Push Button 6*6*5mm 4pin - <1 €
* PLA 3D Print material
* 4 short wires
* 2 screws for USB Module, 4 Screws for the case
* Soldering gears
* Some isolation tape to protect electronic parts

###First, lets buid the hardware:

__1. Connect Usb Module with Raspberry Pi__
<br>
<br>
1.1. Take wires, Usb Module, and Rpi and solder it together like the images shown
<br>
![Which pin where](./images/WifiServer_0.jpg)
<br>
1.2. Use isolation tape to avoid short circuit. You do not need to hot clue the use onto the RPi board, if you bought a Usb module with board.
<br>
This can be srewed to the case later on.
<br>
![After soldering](./images/WifiServer_1-scaled.jpg)

<br>

__2. The Shutdown Button__
<br>
<br>
2.1. Cut the unused legs off. Check the image which ones. I used a multimeter to test the currency flow to be sure.
<br>
![Cut legs off](./images/WifiServer_2.jpg)
<br>
2.2. Plug the remaining legs into the GPIO pin holes like shown. Red is GPIO20, which is used by the OS to trigger the shutdown, Black is Ground. Use some isolation tape between the cut-off legs of the button and the board to avoid short circuit.
<br>
![Plug in the switch](./images/WifiServer_3-e1577198848479.jpg)
<br>
2.3. Solder the legs to the RPi board. The board should now look like shown in the image below:
<br>
![Solder the legs](./images/WifiServer_4-e1577198873867.jpg)
<br>

__3. The Printed Case case__

<br>

__In case you do not have a 3D printer, there are ways to build a case without it__
<br>
Example: [Tic Tac Case](https://www.instructables.com/id/Pi-Zero-W-Tic-Tac-Case/)
<br>
<br>
#### Create the case using a 3D printer
3.1. Load this 2 STL files one after another into Slic3r or your preferred slicing software.
<br>
*(I used PLA, a 0.4 Nozzle, and scaled models up 1% as counteract to shrinking process while cooling)*
<br>
![Case Design](./images/WifiServer_5.jpg)
<br>

3.2. Test after printing, if it fits, __but do not screw it together__. We need to install the OS and slot in the SD card.
<br>

### Secondly, lets install the software
<br>
1. Download and install [Etcher](https://www.balena.io/etcher/)
<br>
2. Download and extract the [WOND SD Image](https://www.dropbox.com/s/fwp3svqvo5iucsj/WOND_FoxDotSpot_v0.1.tar?dl=1)
<br>
3. Connect an SD card reader with the SD card inside.
<br>
4. Open balenaEtcher and select from your hard drive the WifiServer_...img
<br>
5. Select the SD card you wish to write the image to.
<br>
6. Review your selections and click 'Flash!' to begin writing data to the SD card.
<br>
7. Take the SD card out of the reader and slice it into the RPi slot.
<br>
8. Run in the first time, before you close the case
<br>

### Thirdly, in to the case and done:
<br>
* If everything, worked out, you should see the green LED flickering, after you plugged in the dongle
<br>
* Wait till the green LED lights up permanent
<br>
![Green Light](./images/WifiServer_6-e1577203145124.jpg)
<br>

* Now, you should see __FoxDotSpot__ in available Wifi Connections
* Click on it, and use the password: __jamsession2020__

<br>

__Now you should be able to start using Troop__
<br>

In case you want to access the GUI of the offline server as administrator (e.g. changing password)
<br>

1. Open a browser and type in 10.3.141.1
<br>
2. Login with user name: "admin", and password: "jamadmin2020"

<br>
In case you want to access Raspbian via ssh connection, you will need following details:
<br>

Hostname: __wifiserver__
<br>
Login: __ds__
<br>
Password: __offline2020__
<br>

If you want to get internet for the FoxDotSpot Dongle, you will need to create a Hotspot on your phone or computer with
following details (This settings are manually set in Raspbian OS of FoxDotSpot Dongle)
<br>
SSID: __4RPiZero__
<br>
Password: __fe3556d042ec__
<br>

***Be aware this option remains untested. It has been used to install and configure the dongle OS***
